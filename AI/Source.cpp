#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
#include <nlohmann/json.hpp>

using namespace std;

class PenguinModel
{
    class FeelingState
    {
        public:
            vector<double> feelingState;

            FeelingState() {
                feelingState.reserve(3);
            }

            void setActionAuthor(vector<double> inputFeelingState)
            {
                copy(feelingState.begin(), inputFeelingState.end(), back_inserter(inputFeelingState));
            }

            vector<double> getBodyFactorForTarget()
            {
                return feelingState;
            }
    };

    class Act
    {
        public:
            vector<double> bodyFactorForTarget;
            vector<double> moralFactorForTarget;
            vector<double> moralFactorForAuthor;
            string name;
            string responseActionOn;
            string actionAuthor;

            Act()
            {
                bodyFactorForTarget.reserve(5);
                moralFactorForTarget.reserve(3);
                moralFactorForAuthor.reserve(3);
            }
            void setBodyFactorForTarget(vector<double> values)
            {
                copy(bodyFactorForTarget.begin(), values.end(), back_inserter(values));
            }
            void setMoralFactorForTarget(vector<double> values)
            {
                copy(moralFactorForTarget.begin(), values.end(), back_inserter(values));
            }
            void setMoralFactorForAuthor(vector<double> values)
            {
                copy(moralFactorForAuthor.begin(), values.end(), back_inserter(values));
            }

            void setName(string name)
            {
                this->name = name;
            }
            void setResponseActionOn(string responseActionOn)
            {
                this->responseActionOn = responseActionOn;
            }

            void setActionAuthor(string actionAuthor)
            {
                this->actionAuthor = actionAuthor;
            }

            vector<double> getBodyFactorForTarget()
            {
                return bodyFactorForTarget;
            }
            vector<double> getMoralFactorForTarget()
            {
                return moralFactorForTarget;
            }

            vector<double> getMoralFactorForAuthor()
            {
                return moralFactorForAuthor;
            }

            string getName()
            {
                return name;
            }

            string getResponseActionOn()
            {
                return responseActionOn;
            }

            string getActionAuthor()
            {
                return actionAuthor;
            }

    };


    public:
        const double r1 = 0.3;
        bool unstableRelations = true;
        //string JSON_PATH_ALL_ACTIONS = Application.streamingAssetsPath + "\\Actions.json";
        //string JSON_PATH_INDEPENDENT_PENGUIN_ACTIONS = Application.streamingAssetsPath + "\\IndependentPenguinActions.json";
        //string JSON_PATH_INDEPENDENT_FEELINGS_STATES = Application.streamingAssetsPath + "\\FeelingsStates.json";
    
        const double r = 1e-1;
        const double criticalValueForDiffNorms = 0.32;
        

        int countHumanActions = 0;
        bool processRecoveryOfFeelings = false;
        string humanCharacteristic = "NAN";
        string penguinCharacteristic = "NAN";

        map<string, Act> allActs;
        map<string, Act> allIndependentActions;
        map<string, FeelingState> feelingsStates;
        vector<pair<string, double>> somaticLikelihood;
        vector<pair<string, double>> biasLikelihood;

        vector<double> penguinAppraisals;
        vector<double> humanAppraisals;
        vector<double> penguinFeelings;
        vector<double> humanFeelings;
        vector<double> penguinSomaticFactor;

        PenguinModel() {
            penguinAppraisals.reserve(3);
            humanAppraisals.reserve(3);
            penguinFeelings.reserve(3);
            humanFeelings.reserve(3);
            penguinSomaticFactor.reserve(5);
        }

        double TimerToEat = 0;
        bool timetoeat = false;
        bool timetoplay = false;
        double TimerToPlay = 0;

        const double k = 0.1;

    //private void Start()
    //{
    //    penguinai = GetComponent<penguinAI>();
    //}

        vector<double> getPenguinAppraisals()
        {
            return penguinAppraisals;
        }
        vector<double> getPenguinBodyFactor()
        {
            return penguinSomaticFactor;
        }

        vector<double> getPenguinFeelings()
        {
            return penguinFeelings;
        }
        vector<double> getHumanAppraisals()
        {
            return humanAppraisals;
        }
        vector<double> getHumanFeelings()
        {
            return humanFeelings;
        }

        string getHumanCharacteristic()
        {
            return humanCharacteristic;
        }

        enum body
        {
            pain = 0,
            energy = 1,
            satiety = 2,
            activity = 3,
            sociability = 4
        };

        enum moral
        {
            valence = 0,
            arousal = 1,
            dominance = 2
        };

        static vector<double> getNewSomaticFactor(vector<double> body, vector<double> action)
        {
            vector<double> resultBody;
            for (int i = 0; i < body.size(); ++i)
            {
                resultBody.push_back(body[i] + action[i]);
            }
            return resultBody;
        }

        vector<double> getNewAppraisals(vector<double> appraisals, vector<double> action)
        {
            vector<double> resultAppraisals;
            for (int i = 0; i < appraisals.size(); ++i)
            {
                resultAppraisals.push_back((1.0 - r) * appraisals[i] + r * action[i]);
            }
            return resultAppraisals;
        }

        void penguinMakeAct(string action)
        {
            penguinAppraisals = getNewAppraisals(penguinAppraisals, allIndependentActions[action].getMoralFactorForTarget());
            humanAppraisals = getNewAppraisals(humanAppraisals, allIndependentActions[action].getMoralFactorForAuthor());
            penguinSomaticFactor = getNewSomaticFactor(penguinSomaticFactor, allIndependentActions[action].getBodyFactorForTarget());
        }
        class Penguin
        {
            public:
                vector<double> bodyFactor;
                vector<double> moralFactor;
                Penguin()
                {
                    bodyFactor.reserve(5);
                    moralFactor.reserve(3);
                }

        };


        void setupActs()
        {
            //feelingsStates = JsonConvert.DeserializeObject<Dictionary<string, FeelingState>>(File.ReadAllText(JSON_PATH_INDEPENDENT_FEELINGS_STATES));
            //allActs = JsonConvert.DeserializeObject<Dictionary<string, Act>>(File.ReadAllText(JSON_PATH_ALL_ACTIONS));
            //allIndependentActions = JsonConvert.DeserializeObject<Dictionary<string, Act>>(File.ReadAllText(JSON_PATH_INDEPENDENT_PENGUIN_ACTIONS));
        }

        void isRelationsUnstable(vector<double> feelings)
        {
            string humanCharacteristic = findHumanFeelingCharacteristick(feelings);
            double dif = 10;
            for (int i = 0; i < 3; ++i)
            {
                if (feelingsStates[humanCharacteristic].feelingState[i] != 0)
                {
                    dif = min(abs(feelings[i] - feelingsStates[humanCharacteristic].feelingState[i]), dif);
                }
            }
            if (dif < 0.15)
            {
                unstableRelations = false;
            }
        }

        vector<double> getNewFeelingFactor(vector<double> feelings, vector<double> appraisals)
        {
            isRelationsUnstable(feelings);
            if (unstableRelations)
            {
                feelings = firstMethorRebuildFeelings(feelings, appraisals);
                return feelings;
            }
            double diffNorm = 0;
            for (int i = 0; i < 3; ++i)
            {
                diffNorm += abs(feelings[i] - appraisals[i]);
            }
            processRecoveryOfFeelings = diffNorm > criticalValueForDiffNorms;
            if (processRecoveryOfFeelings)
            {
                feelings = secondMethodRebuildFeelings(feelings, appraisals);
            }
            else
            {
                feelings = setConstantFeelings(feelings);
            }
            return feelings;
        }

        vector<double> firstMethorRebuildFeelings(vector<double> feelings, vector<double> appraisals)
        {
            vector<double> resultFeelings;
            for (int i = 0; i < 3; i++)
            {
                resultFeelings.push_back(1.1 * appraisals[i]);
            }
            return resultFeelings;
        }

        vector<double> secondMethodRebuildFeelings(vector<double> feelings, vector<double> appraisals)
        {
            PrintToConsole("secondMethodRebuildFeelings");
            vector<double> resultFeelings;
            for (int i = 0; i < 3; ++i)
            {
                resultFeelings.push_back((1 - r1) * feelings[i] + r1 * (appraisals[i] - feelings[i]));
            }
            humanCharacteristic = "NAN";
            return resultFeelings;
        }

        string findHumanFeelingCharacteristick(vector<double> feelings)
        {
            string choise = "";
            double dif = 20;
            for (map<string, FeelingState>::iterator feelingMassive = feelingsStates.begin(); feelingMassive != feelingsStates.end(); feelingMassive++)
            {
                for (int i = 0; i < 3; ++i)
                {
                    if (feelingMassive->second.feelingState[i] != 0)
                    {
                        double mid = abs(feelingMassive->second.feelingState[i] - feelings[i]);
                        if (mid < dif)
                        {
                            dif = mid;
                            choise = feelingMassive->first;
                        }
                    }
                }
            }
            return choise;
        }

        vector<double> setConstantFeelings(vector<double> feelings)
        {
            PrintToConsole("setConstantFeelings");
            humanCharacteristic = findHumanFeelingCharacteristick(feelings);
            vector<double> ans(3);
            copy(ans.begin(), feelingsStates[humanCharacteristic].feelingState.end(), back_inserter(feelingsStates[humanCharacteristic].feelingState));
            return ans;
        }
        void somaticCriterion(vector<double> penguinBodyFactor, string action)
        {
            double maxValue = 0;
            double mainNorm = 0;
            double recNorm = 0;
            for (map<string, Act>::iterator characterAction = allActs.begin(); characterAction != allActs.end(); characterAction++)
            {
                double normAfterAction = 0;
                auto currentAction = characterAction->second;
                if (currentAction.getResponseActionOn() == action)
                {
                    for (int i = 0; i < penguinBodyFactor.size(); ++i)
                    {
                        normAfterAction += pow(penguinBodyFactor[i] + currentAction.getBodyFactorForTarget()[i], 2);
                    }
                    somaticLikelihood.push_back(make_pair(currentAction.getName(), normAfterAction));
                    mainNorm += normAfterAction;
                    maxValue = max(maxValue, abs(normAfterAction));
                }
            }
            for (int i = 0; i < somaticLikelihood.size(); ++i)
            {
                somaticLikelihood[i] = make_pair(somaticLikelihood[i].first, 1 - somaticLikelihood[i].second / mainNorm);
                recNorm += somaticLikelihood[i].second;
                //somaticLikelihood[i] = new Tuple<string, double>(somaticLikelihood[i].Item1, maxValue - somaticLikelihood[i].Item2);
            }
            for (int i = 0; i < somaticLikelihood.size(); ++i)
            {
                somaticLikelihood[i] = make_pair(somaticLikelihood[i].first, somaticLikelihood[i].second / recNorm);
                PrintToConsole("somatic norms for " + somaticLikelihood[i].first + " " + to_string(somaticLikelihood[i].second));
            }
        }

        void biasCriterion(vector<double> appraisalsFactor, vector<double> feelingsFactor, string action)
        {
            double maxValue = 0;
            double mainNorm = 0;
            double recNorm = 0;
            for (map<string, Act>::iterator el = allActs.begin(); el != allActs.end(); el++)
            {
                double difference = 0;
                auto responseAction = el->second;
                if (responseAction.getResponseActionOn() == action)
                {
                    auto appraisalsAfterAction = getNewAppraisals(appraisalsFactor, responseAction.getBodyFactorForTarget());
                    for (int i = 0; i < feelingsFactor.size(); ++i)
                    {
                        difference += pow(feelingsFactor[i] - appraisalsAfterAction[i], 2);
                    }
                    biasLikelihood.push_back(make_pair(responseAction.getName(), difference));
                    mainNorm += difference;
                    maxValue = max(maxValue, difference);
                }
            }
            for (int i = 0; i < biasLikelihood.size(); ++i)
            {
                biasLikelihood[i] = make_pair(biasLikelihood[i].first, 1 - biasLikelihood[i].second / mainNorm);
                recNorm += biasLikelihood[i].second;
                // biasLikelihood[i] = new Tuple<string, double>(biasLikelihood[i].Item1, maxValue - biasLikelihood[i].Item2);
            }
            for (int i = 0; i < biasLikelihood.size(); ++i)
            {
                biasLikelihood[i] = make_pair(biasLikelihood[i].first, biasLikelihood[i].second / recNorm);
                PrintToConsole("bias norms for " + biasLikelihood[i].first + " " + to_string(biasLikelihood[i].second));
            }
        }

        void rebuildStatesAfterHumanAction(string humanAct)
        {
            humanAppraisals = getNewAppraisals(humanAppraisals, allActs[humanAct].getMoralFactorForAuthor()); // ��������� ��������� �������� ����� ��������
            humanFeelings = getNewFeelingFactor(humanFeelings, humanAppraisals);

            penguinAppraisals = getNewAppraisals(penguinAppraisals, allActs[humanAct].getMoralFactorForTarget());
            penguinSomaticFactor = getNewSomaticFactor(penguinSomaticFactor, allActs[humanAct].getBodyFactorForTarget());
            //penguinFeelings = getNewFeelingFactor(penguinFeelings, penguinAppraisals); //��������� ��������� �������� ����� ��������
            PrintToConsole("humanCharacteristic = " + humanCharacteristic);
            mitAppraisalsAndFeelings();
        }

        void rebuildStatesAfterPenguinAction(string penguinAct)
        {
            humanAppraisals = getNewAppraisals(humanAppraisals, allActs[penguinAct].getMoralFactorForTarget()); // ��������� ��������� �������� ����� ��������
            humanFeelings = getNewFeelingFactor(humanFeelings, humanAppraisals);

            penguinAppraisals = getNewAppraisals(penguinAppraisals, allActs[penguinAct].getMoralFactorForAuthor());
            penguinSomaticFactor = getNewSomaticFactor(penguinSomaticFactor, allActs[penguinAct].getBodyFactorForTarget());
            //penguinFeelings = getNewFeelingFactor(penguinFeelings, penguinAppraisals); //��������� ��������� �������� ����� ��������
            PrintToConsole("humanCharacteristic = " + humanCharacteristic);
            mitAppraisalsAndFeelings();
        }

        void PrintToConsole(string output) {
            cout << output << endl;
        }

        string getResponseAction()
        {
            string response = "";
            vector<pair<string, double>> result;
            for (auto somaticEl : somaticLikelihood)
            {
                for (auto biasEl : biasLikelihood)
                {
                    if (somaticEl.first == biasEl.first)
                    {
                        result.push_back(make_pair(somaticEl.first, biasEl.second + somaticEl.second * k));
                    }
                }
            }
            double actionLikelihood = 0;
            for (auto el : result)
            {
                if (el.second > actionLikelihood)
                {
                    actionLikelihood = el.second;
                    response = el.first;
                }
            }
            return response;
        }

        bool sortbysec(const pair<int, int>& a,
            const pair<int, int>& b)
        {
            return (a.second < b.second);
        }

        string getResponseActionByLikelihood()
        {
            string response = "";
            vector<pair<string, double>> result;
            double sum = 0;
            for (auto somaticEl : somaticLikelihood)
            {
                for (auto biasEl : biasLikelihood)
                {
                    if (somaticEl.first == biasEl.first)
                    {
                        double likelihood = biasEl.second + somaticEl.second * k;
                        result.push_back(make_pair(somaticEl.first, likelihood));
                        sum += likelihood;
                    }
                }
            }
            for (int i = 0; i < result.size(); ++i)
            {
                result[i] = make_pair(result[i].first, result[i].second / sum);
            }
            sort(result.begin(), result.end(), [](const pair<string, double>& left, const pair<string, double>& right) {
                return left.second < right.second;
                });
            double actionLikelihood = rand() % 10000 / 10000.0;
            double currentLikelihood = 0;
            for (auto el : result)
            {
                currentLikelihood += el.second;
                if (currentLikelihood >= actionLikelihood)
                {
                    response = el.first;
                    break;
                }
            }
            return response;
        }

        vector<double> limitVecs(vector<double> vec)
        {
            for (int i = 0; i < vec.size(); ++i)
            {
                vec[i] = min(vec[i], 0.5);
                vec[i] = max(vec[i], -0.5);
            }
            return vec;
        }

        void mitAppraisalsAndFeelings()
        {
            humanAppraisals = limitVecs(humanAppraisals);
            humanFeelings = limitVecs(humanFeelings);
            penguinAppraisals = limitVecs(penguinAppraisals);
            penguinFeelings = limitVecs(penguinFeelings);
        }

        string launchNewScene(string humanAct)
        {
            somaticLikelihood;
            biasLikelihood;

            rebuildStatesAfterHumanAction(humanAct);
            somaticCriterion(penguinSomaticFactor, humanAct);
            biasCriterion(humanAppraisals, humanFeelings, humanAct);
            string answer = getResponseActionByLikelihood();
            rebuildStatesAfterPenguinAction(answer);
            return answer;
        }
};