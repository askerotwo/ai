#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <string>

using namespace std;

class Logging {
	private:
		void print(map <string, vector<double>> myMap, string fileName, string action, int actionNumber) {
			ofstream out;
			out.open(fileName, ios::binary | ios::app);
			if (out.is_open()) {
				out << action << " : ";
				for (auto el : myMap) {
					out << el.first << ", ";
					for (int index = 0; index < el.second.size(); ++index) {
						out << el.second[index];
						if (index != el.second.size() - 1) {
							out << ", ";
						}
					}
					out << " : ";
				}
				out << endl;
			}
		}

	public:
		static int number;

		void printAppraisalsAndFeelings(map <string, vector<double>> appraisals, map <string, vector<double>> feelings, string action, int actionNumber) {
			print(appraisals, "C:\\Users\\��������\\source\\repos\\AI\\AI\\logs\\log_appraisals" + to_string(number) + ".txt", action, actionNumber);
			print(feelings, "C:\\Users\\��������\\source\\repos\\AI\\AI\\logs\\log_feelings" + to_string(number) + ".txt", action, actionNumber);
		}
};