#include <iostream>
#include <vector>
#include "GlobalVariables.cpp"

//����� �������
//�������� �� �������� ������ ������������� � ��������

//replicaIndex - ����� �������-������ ������������� � ��������
//agentType - ��� ��� ������� (�������� ��� �������������)
//utterance - ����� �����
//vectorVad - valence, arousal, dominance ��� ������ �������

using namespace std;

class Replica {
	private:
		int replicaIndex;
		string agentType;
		int number;
		vector<pair<Translate, string>> utterance;
		vector <double> vectorVADFrom;
		vector <double> vectorVADTo;

		string getUtteranceForLanguage(Translate language) {
			for (auto el : this->utterance) {
				if (el.first == language) {
					return el.second;
				}
			}
			return "NO TRANSLATIONS FOUND";
		}

	public:
		Replica(pair<Translate, string> utterance) {
			this->utterance.push_back(utterance);
			this->vectorVADFrom = { 0, 0, 0 };
			this->vectorVADTo = { 0, 0, 0 };
		}

		Replica(int replicaIndex, int number, string agentType, pair<Translate, string> utteranceEN, vector <double> vectorVADFrom, vector <double> vectorVADTo) {
			this->number = number;
			this->replicaIndex = replicaIndex;
			this->agentType = agentType;
			this->utterance.push_back(utteranceEN);
			copy(vectorVADFrom.begin(), vectorVADFrom.end(), back_inserter(this->vectorVADFrom));
			copy(vectorVADTo.begin(), vectorVADTo.end(), back_inserter(this->vectorVADTo));
		}

		int getNumber() {
			return number;
		}

		void setNumber(int number) {
			this->number = number;
		}


		int getReplicaIndex() {
			return replicaIndex;
		}

		string getAgentType() {
			return agentType;
		}

		string getUtteranceEN() {
			return getUtteranceForLanguage(Translate::en);
		}

		string getUtteranceRUS() {
			return getUtteranceForLanguage(Translate::ru);
		}

		string getUtterance() {
			return getUtteranceForLanguage(GlobalVariables::language);
		}

		vector<double> getVectorVADFrom() {
			return vectorVADFrom;
		}

		vector<double> getVectorVADTo() {
			return vectorVADTo;
		}

		void setReplicaIndex(int replicaIndex) {
			this->replicaIndex = replicaIndex;
		}

		void setAgentType(string agentType) {
			this->agentType = agentType;
		}

		void setUtterance(pair<Translate, string> utterance) {
			for (auto& el : this->utterance) {
				if (el.first == utterance.first) {
					el.second = utterance.second;
					return;
				}
			}
			this->utterance.push_back(utterance);
		}

		void setVectorVADFrom(vector <double> vectorVAD) {
			copy(vectorVAD.begin(), vectorVAD.end(), back_inserter(this->vectorVADFrom));
		}

		void setVectorVADTo(vector <double> vectorVAD) {
			copy(vectorVAD.begin(), vectorVAD.end(), back_inserter(this->vectorVADTo));
		}
		
};