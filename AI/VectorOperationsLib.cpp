#include <vector>
#include <iostream>

using namespace std;

class VectorOperationsLib {
public:
	static vector <double> sumOfTwoVectors(vector <double> first, vector <double> second) {
		vector <double> res;
		for (int index = 0; index < first.size(); ++index) {
			res.push_back(first[index] + second[index]);
		}
		return res;
	}

	static vector <double> difOfTwoVectors(vector <double> first, vector <double> second) {
		vector <double> res;
		for (int index = 0; index < first.size(); ++index) {
			res.push_back(first[index] - second[index]);
		}
		return res;
	}

	static vector <double> multipleConstantOnVector(double constNumber, vector <double> first) {
		vector <double> res;
		for (int index = 0; index < first.size(); ++index) {
			res.push_back(constNumber * first[index]);
		}
		return res;
	}

	static vector <double> devideVectorOnConstant(double constNumber, vector <double> first) {
		vector <double> res;
		for (int index = 0; index < first.size(); ++index) {
			res.push_back(first[index] / constNumber);
		}
		return res;
	}

	static vector <double> getVectorWithPrefixSums(vector <double> first) {
		for (int index = 1; index < first.size(); ++index) {
			first[index] += first[index - 1];
		}
		return first;
	}

	static double moduleDiffBetweenVectors(vector <double> first, vector<double> second) {
		double diff = 0;
		for (int index = 0; index < first.size(); ++index) {
			diff += pow(abs(first[index] - second[index]),2);
		}
		return sqrt(diff);
	}

	static int findIndexOfMinNumberInVector(vector <double> first) {
		int index = 0;
		double minEl = first[0];
		for (int i = 1; i < first.size(); ++i) {
			if (first[i] < minEl) {
				minEl = first[i];
				index = i;
			}
		}
		return index;
	}

	static void printVector(vector <double> first) {
		for (auto el : first) {
			cout << el << " ";
		}
		cout << endl;
	}

	static double findSquareOfVector(vector <double> first) {
		double res = 0;
		for (auto el : first) {
			res += pow(el, 2);
		}
		return sqrt(res);
	}

};