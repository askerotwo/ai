#include <vector>
#include "VectorOperationsLib.cpp"
#include "Loader.cpp"

using namespace std;

class MoralSchama {

	private:
		vector <ConstantFeelingsState> allConstantFeelingStates;
		Loader loader;
		boolean isRelationshipStable = false;
		const double difForStableRelationship = 0.8;
		const double difForRecovery = 0.01;
		const double beta = 1.1;
		const double r1 = 0.15;
		const double r = 0.1;

		vector <double> firstMode(vector <double> feelings) {
			return feelings = VectorOperationsLib::multipleConstantOnVector(beta, feelings);
		}

		vector <double> secondMode(vector <double> feelings) {
			vector <double> diffs;
			for (int index = 0; index < allConstantFeelingStates.size(); ++index) {
				diffs.push_back(VectorOperationsLib::moduleDiffBetweenVectors(feelings, allConstantFeelingStates[index].vectorVAD));
			}
			return feelings = allConstantFeelingStates[VectorOperationsLib::findIndexOfMinNumberInVector(diffs)].vectorVAD;
		}

		vector <double> thirdMode(vector <double> feelings, vector <double> appraisals) {
			return feelings = VectorOperationsLib::sumOfTwoVectors(
				VectorOperationsLib::multipleConstantOnVector(1 - r1, feelings), VectorOperationsLib::multipleConstantOnVector(r1, VectorOperationsLib::difOfTwoVectors(appraisals, feelings)));
		}

		double getAbsDiffAprraisalsAndFeelings(vector <double>& feelings, vector<double>& appraisals) {
			double diff = VectorOperationsLib::findSquareOfVector(VectorOperationsLib::difOfTwoVectors(appraisals, feelings));
			if (diff < difForStableRelationship) {
				isRelationshipStable = true;
			}
			return diff;
		}

		
	public:
		MoralSchama() {
			this->allConstantFeelingStates = loader.setUpConstantFeelingStatesJSON();
		}

		vector<double> getNewFeelings(vector <double> feelings, vector<double> appraisals) {
			double diff = getAbsDiffAprraisalsAndFeelings(feelings, appraisals);
			if (!isRelationshipStable) {
				return firstMode(feelings);
			}
			if (diff < difForRecovery) {
				return secondMode(feelings);
			}
			else {
				return thirdMode(feelings, appraisals);
			}
		}

		vector<double> getNewAppraisals(vector <double> appraisals, vector <double> action) {
			return VectorOperationsLib::sumOfTwoVectors(
				VectorOperationsLib::multipleConstantOnVector(1 - r, appraisals), VectorOperationsLib::multipleConstantOnVector(r, action));
		}
};