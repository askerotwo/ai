#include <iostream>
#include <vector>
#include <map>
#include "MoralSchema.cpp"
#include <random>
#include "Logging.cpp"

using namespace std;

class CognitiveModel {
	private:
		int replicaIndex;
		string agentType;

		MoralSchama moralSchema;
		Loader loader;
		Logging logging;

		map <string, vector<double>> appraisals;
		map <string, vector<double>> feelings;

		string getOppositeActor() {
			if (agentType == "V") {
				return "R";
			}
			return "V";
		}

		Replica getReplicaByBiasLikelihood() {
			vector <Replica> availableReplicas = getAvailablesReplicas();
			if (availableReplicas.size() == 0) {
				return Replica(make_pair(Translate::en, "\n")); //временная заглушка
			}
			vector <double> likelihoods;
			double sumOfLikelihoods = 0;
			for (int index = 0; index < availableReplicas.size(); ++index) {
				vector <double> apparisalsAfterAction = moralSchema.getNewAppraisals(appraisals.find(agentType)->second, availableReplicas[index].getVectorVADFrom());
				likelihoods.push_back(VectorOperationsLib::moduleDiffBetweenVectors(feelings.find(agentType)->second, apparisalsAfterAction));
				sumOfLikelihoods += likelihoods[index];
			}
			likelihoods = VectorOperationsLib::getVectorWithPrefixSums(VectorOperationsLib::devideVectorOnConstant(sumOfLikelihoods, likelihoods));
			srand(time(NULL));
			mt19937 mt_rand(time(0));
			double decision = (mt_rand() % 10000) / 10000.0;
			for (int index = 0; index < likelihoods.size(); ++index) {
				if (decision < likelihoods[index]) {
					return availableReplicas[index];
				}
			}
		}

		vector<Replica> getAvailablesReplicas() {
			vector <Replica> availableReplicas;
			for (auto el : allReplicas) {
				if (el.getAgentType() == agentType && el.getReplicaIndex() == replicaIndex) {
					availableReplicas.push_back(el);
				}
			}
			return availableReplicas;
		}

		std::pair<string, int> choosenAction() {
			Replica replica = getReplicaByBiasLikelihood();
			recalcFeelingsAppraisals(replica);
			return make_pair(replica.getUtterance(), replica.getNumber());
		}

		void recalcFeelingsAppraisals(Replica replica) {
			appraisals.find(agentType)->second = moralSchema.getNewAppraisals(appraisals.find(agentType)->second, replica.getVectorVADFrom());
			feelings.find(agentType)->second = moralSchema.getNewFeelings(feelings.find(agentType)->second, appraisals.find(agentType)->second);

			appraisals.find(getOppositeActor())->second = moralSchema.getNewAppraisals(appraisals.find(getOppositeActor())->second, replica.getVectorVADTo());
			feelings.find(getOppositeActor())->second = moralSchema.getNewFeelings(feelings.find(getOppositeActor())->second, appraisals.find(getOppositeActor())->second);
		}
	
	public:
		vector <Replica> allReplicas;

		CognitiveModel() {
			allReplicas = loader.setUpReplicasJSON();
			appraisals.insert(pair<string, vector<double> >("V", { 0, 0, 0 }));
			appraisals.insert(pair<string, vector<double> >("R", { 0, 0, 0 }));
			feelings.insert(pair<string, vector<double> >("V", { 0, 0, 0 }));
			feelings.insert(pair<string, vector<double> >("R", { 0, 0, 0 }));
		}

		void recalcAppraisalsAfterHumanAction(string action) {
			Replica replica = findReplicaByUtterance(action);
			recalcFeelingsAppraisals(replica);
		}

		Replica findReplicaByUtterance(string action) {
			for (auto replica : allReplicas) {
				if (replica.getUtterance() == action) {
					return replica;
				}
			}
		}

		string getAction(int replicaIndex, string agentType = "V") {
			this->replicaIndex = replicaIndex;
			this->agentType = agentType;

			auto action = choosenAction();

			logging.printAppraisalsAndFeelings(appraisals, feelings, action.first, action.second);

			return action.first;
		}
};