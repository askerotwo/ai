#include <vector>
#include <string>

using namespace std;

class ConstantFeelingsState {
	public:
		string name;
		vector <double> vectorVAD;

		ConstantFeelingsState(string name, vector<double> vectorVAD) {
			this->name = name;
			this->vectorVAD = vectorVAD;
		}
};