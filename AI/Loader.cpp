#include <iostream>
#include <vector>
#include <nlohmann/json.hpp>
#include <fstream>
#include <Windows.h>
#include "Replica.cpp"
#include "ConstantFeelingState.cpp"

using json = nlohmann::json;
using namespace std;

//����� �������� �� ������������� ��������� ������

class Loader {
    private:
        void setUpRusTranslation(vector <Replica>& allReplicas) {
            ifstream in("translations_en_rus.txt");

            string inpString;
            while (getline(in, inpString)) {
                string utteranceEN = inpString.substr(0, inpString.find('@'));
                string utteranceRUS = inpString.substr(inpString.find('@') + 1, inpString.size() - 1 - inpString.find('@'));
                for (auto& el : allReplicas) {
                    if (el.getUtteranceEN() == utteranceEN) {
                      
                        el.setUtterance(make_pair(Translate::ru, utteranceRUS));
                    }
                }
            }
        }
	public:
        Loader() {

        }

        //���������� ������ �� json �����
		vector <Replica> setUpReplicasJSON() {
            vector <Replica> allReplicas;
            ifstream in("json_data.json");
            json j;
            in >> j;
            for (auto& replica : j["objects"]) {
                int replicaIndex = replica["replica_index"].get<int>();
                int number = std::stoi(replica["number"].get<string>());
                string agentType = replica["agent"].get<string>();
                string utteranceEN = replica["utteranceEN"].get<string>();
                vector <double> vectorVADFrom;
                vector <double> vectorVADTo;
                
                for (auto& vadVectorEl : replica["vad_array_from"]) {
                    vectorVADFrom.push_back(vadVectorEl.get<double>());
                }

                for (auto& vadVectorEl : replica["vad_array_to"]) {
                    vectorVADTo.push_back(vadVectorEl.get<double>());
                }

                allReplicas.push_back(Replica(replicaIndex, number, agentType, make_pair(Translate::en, utteranceEN), vectorVADFrom, vectorVADTo));
            }
            setUpRusTranslation(allReplicas);
            return allReplicas;
		}

        vector <ConstantFeelingsState> setUpConstantFeelingStatesJSON() {
            vector <ConstantFeelingsState> allConstantFeelingsState;
            ifstream in("feelings_state.json");
            json j;
            in >> j;
            for (auto& state : j["states"]) {
                string name = state["name"].get<string>();
                vector <double> vectorVAD;
                for (auto& vadVectorEl : state["vad_array"]) {
                    vectorVAD.push_back(vadVectorEl.get<double>());
                }
                allConstantFeelingsState.push_back(ConstantFeelingsState(name, vectorVAD));
            }
            return allConstantFeelingsState;
        }
};